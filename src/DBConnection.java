import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSetMetaData;
import com.mysql.jdbc.Statement;

public class DBConnection
{
	static Connection conn;
	static Statement statement;
	static ResultSet resultSet;
	static int updatedResults;
	

	public DBConnection() throws SQLException
	{

		conn = ( Connection ) DriverManager.getConnection( "jdbc:mysql://10.14.23.41:3306/games24x7v3", "gwportal", "gwportal" );
		statement = ( Statement ) conn.createStatement();
	}

	public String makeConnection( String query, String columnName ) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{

		String result = null;

		try
		{

			Class.forName( "com.mysql.jdbc.Driver" ).newInstance();

			resultSet = statement.executeQuery( query );

			while( resultSet.next() )
			{
				result = resultSet.getString( columnName );
			}

		}
		catch( Exception e )
		{

		}

		return result;

	}

	public List<Long> getUserList( String query ) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{

		List<Long> result = new ArrayList() ;

		Class.forName( "com.mysql.jdbc.Driver" ).newInstance();

		resultSet = statement.executeQuery( query );

		while( resultSet.next() )
		{

			 result.add( resultSet.getLong( "user_id" ) );
			

		}

		return result;

	}
	
	public int getUpdates(String query) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		
		Class.forName( "com.mysql.jdbc.Driver" ).newInstance();

		updatedResults = statement.executeUpdate( query );
		
		return updatedResults;
		
		
		
	}
	
	
	

}
