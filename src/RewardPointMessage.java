

import java.sql.Timestamp;
import java.util.List;

public class RewardPointMessage
{
    private String source;
    private String dest;
    private int credit_debit;
    private List<Long> userids;
    private long rewardPoints;
    private int txn_id;
    private Timestamp sys_time;
    private String comments;
    
    public RewardPointMessage()
    {
        super();
    }
    
    public RewardPointMessage( String source, String dest, int credit_debit, List< Long > userids, long rewardPoints, int txn_id, Timestamp sys_time, String comments)
    {
        super();
        this.source = source;
        this.dest = dest;
        this.credit_debit = credit_debit;
        this.userids = userids;
        this.rewardPoints = rewardPoints;
        this.txn_id = txn_id;
        this.sys_time = sys_time;
        this.comments = comments;
        
    }
    public String getSource()
    {
        return source;
    }
    public void setSource( String source )
    {
        this.source = source;
    }
    public String getDest()
    {
        return dest;
    }
    public void setDest( String dest )
    {
        this.dest = dest;
    }
    public int isCredit_debit()
    {
        return credit_debit;
    }
    public void setCredit_debit( int credit_debit )
    {
        this.credit_debit = credit_debit;
    }
    public List< Long > getUserids()
    {
        return userids;
    }
    public void setUserids( List< Long > userids )
    {
        this.userids = userids;
    }
    public long getRewardPoints()
    {
        return rewardPoints;
    }
    public void setRewardPoints( long rewardPoints )
    {
        this.rewardPoints = rewardPoints;
    }
    public int getTxn_id()
    {
        return txn_id;
    }
    public void setTxn_id( int txn_id )
    {
        this.txn_id = txn_id;
    }
    public Timestamp getSys_time()
    {
        return sys_time;
    }
    public void setSys_time( Timestamp sys_time )
    {
        this.sys_time = sys_time;
    }
    public String getComments()
    {
        return comments;
    }
    public void setComments( String comments )
    {
        this.comments = comments;
    }
    @Override
    public String toString()
    {
        return "RewardPointMessage [source=" + source + ", dest=" + dest + ", credit_debit=" + credit_debit + ", userids=" + userids + ", rewardPoints=" + rewardPoints + ", txn_id=" + txn_id
                + ", sys_time=" + sys_time + ", comments=" + comments + "]";
    }
}