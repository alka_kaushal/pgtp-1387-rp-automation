import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.games24x7.common.domainObject.UserContext;
import com.games24x7.format.json.JSONArray;
import com.games24x7.format.json.JSONException;
import com.games24x7.format.json.JSONObject;
import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.games24x7.framework.mq.MQFrameworkFactory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;


public class EDSPublisher
{

	public static String mqPropsFilePath = "/home/tomcat/edsRp/res/mq.props";
	private static PropsFileBasedConfiguration mainConfig;
	private static String EdsNSQueueName = "nfsrp";

	public static void main( String[] args ) throws JSONException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, InterruptedException, IOException
	{
		
		mainConfig = new PropsFileBasedConfiguration( mqPropsFilePath );
		MQFrameworkFactory.init( mainConfig );
		MQFrameworkFactory.getFramework().registerQueuePublisher( "nfsrp" );
		Thread.sleep( 3000 );

		List<Long> userList = new ArrayList();
		DBConnection connection = new DBConnection();
		userList = connection.getUserList(("select user_id from users where user_id>"+mainConfig.getIntValue( "USER_ID" )+" limit " + mainConfig.getIntValue( "NO_OF_USERS" )));
		//Change made for load testing
		/*
		for( long no_of_users = mainConfig.getLongValue( "NO_OF_USERS" ), i = 0; no_of_users > 0; no_of_users--, i++ )
		{
		userList.add(mainConfig.getLongValue("USER_ID")+i);
		}*/
		
		//Change made for production testing
		/*List users = new ArrayList();
		users.add(209);
		users.add(210);*/
		
		java.util.Date date = new java.util.Date();
		RewardPointMessage rpm = new RewardPointMessage( mainConfig.getStringValue( "SOURCE" ), mainConfig.getStringValue( "DEST" ), mainConfig.getIntValue( "CR/DR" ) ,userList, mainConfig.getLongValue( "NOOfRPs" ), mainConfig.getIntValue( "TXN_ID" ), new Timestamp( date.getTime() ), mainConfig.getStringValue( "COMMENTS" ));
		Gson gson = new Gson();
		JsonObject value = new JsonObject();
		value.addProperty( "eventType", "creditRewardPoints" );
		value.addProperty( "value", gson.toJson( rpm ) );
		String messageObject = gson.toJson( value );
		
		

		System.out.println( "Sending" );
		
		/*for(int i=0;i<=mainConfig.getIntValue( "NO_OF_MSGS" );i++){*/
			
			MQFrameworkFactory.getFramework().publishToQueue( "nfsrp", messageObject );	
			System.out.println(messageObject);	
			
		/*}*/	

		Thread.sleep( 3000 );
		System.out.println( "Sent" );
		MQFrameworkFactory.getFramework().close();
		// Check made after the publishing to the queue
		QueryDB.QueryDatabase( mainConfig.getIntValue( "NO_OF_USERS" ), mainConfig.getLongValue( "USER_ID" ) );

	}
}
