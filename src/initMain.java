import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.games24x7.framework.mq.MQFrameworkFactory;



public class initMain {

	public static String mqPropsFilePath = "/home/alka/Documents/EDSChangesForRP/src/eds_mq.props";
	

	public static void main(String[] args) {


		MQFrameworkFactory.init( new PropsFileBasedConfiguration( mqPropsFilePath ) );
		MQFrameworkFactory.getFramework("EDS_EXCHANGE").registerTopicConsumer("edsTopic", new NSConsumer());
		//System.out.println("NS consumer initialized");

	}

}
